#!/usr/bin/env python3
import itertools

MAX_CELL_VOLTAGE = 4.20
MAX_NUMBER_OF_CELLS = 4
ADC_MAX_INPUT = 3.3
EFF_LIMIT = 1.00
PRINT_EFFICIENCY_THRES = 0.9
RESISTOR_LIST = "chipotekaResistorList.txt"


#### do not edit below this line ####

multipliers = dict()
multipliers["R"] = 1
multipliers["k"] = 1000
multipliers["K"] = 1000
multipliers["M"] = 1000000
multipliers["G"] = 1000000000
multipliers["T"] = 1000000000000
multipliers["P"] = 1000000000000000
multipliers["E"] = 1000000000000000000
multipliers["J"] = 1000000000000000000000

multipliers["m"] = 0.001
multipliers["u"] = 0.000001
multipliers["n"] = 0.000000001
multipliers["p"] = 0.000000000001
multipliers["f"] = 0.000000000000001 

all_sufixes = multipliers.keys()


# TODO add support for 4K7-like formats
def resistor_to_value(resistor_string):
    from copy import copy
    
    original_string = copy(resistor_string)
    if resistor_string[-1] in all_sufixes:
            
            multiplier = multipliers[resistor_string[-1]]

        
    elif not resistor_string[-1].isdigit():
        
        raise runtimeError(f'unsupported suffix "{resistor_string[-1]}"')

    resistor_string = resistor_string[0:-1]

    value = None
    if resistor_string.isdigit():
        value = int(resistor_string)

    else:
        try:
            value = float(resistor_string)
        except ValueError:
            raise RuntimeError(f'unexpected resistor value "{original_string}"')
    
    return value * multiplier


def import_resistor_list(path):
    resistor_list = []
    resistor_list_string = []
    with open(path, "r") as resistor_file:
        for raw_value in resistor_file:
            resistance = None        

            raw_value = raw_value.strip()
            if raw_value != "":
                resistor_list_string.append(raw_value)
    
    resistor_list_string = list(set(resistor_list_string))
            
    for r in resistor_list_string:
        resistance = resistor_to_value(r)
        #print(r, resistance)
        resistor_list.append(resistance)
    #input()
    return resistor_list_string, resistor_list


def fitness_string(resistor_high, resistor_low, resistor_high_marking=None, resistor_low_marking=None):
    
    voltage_battery = MAX_CELL_VOLTAGE * MAX_NUMBER_OF_CELLS
    
    max_current = voltage_battery / (resistor_high + resistor_low)
    max_low_voltage = max_current * resistor_low
    max_high_voltage = max_current * resistor_high
    power_low = max_current * max_low_voltage 
    power_high = max_current * max_high_voltage
    power = voltage_battery * max_current
    efficiency = max_low_voltage / ADC_MAX_INPUT

    if resistor_high_marking is None:
        resistor_high_marking = str(resistor_high) + "Ω"
    if resistor_low_marking is None:
        resistor_low_marking = str(resistor_low) + "Ω"
    
    return f'{resistor_high_marking}\t, {resistor_low_marking}\t' + \
        f'-> {max_low_voltage:.3f}V/{ADC_MAX_INPUT:.3f}V ({efficiency*100:.3f}%)' + \
        f' @ {max_current*1000:.3f}mA,{power:.3f}W'
    '(PbigRes:{power_high:.3f} + PsmlRes: {power_low:.3f})'

def fitness(resistor_high, resistor_low):
    voltage_battery = MAX_CELL_VOLTAGE * MAX_NUMBER_OF_CELLS
    
    max_current = voltage_battery / (resistor_high + resistor_low)
    max_low_voltage = max_current * resistor_low
    max_high_voltage = max_current * resistor_high
    power_low = max_current * max_low_voltage 
    power_high = max_current * max_high_voltage
    power = voltage_battery * max_current
    efficiency = max_low_voltage / ADC_MAX_INPUT
    
    if efficiency > EFF_LIMIT:
        efficiency = 0
    
    return efficiency, 1/power


markings, values_r = import_resistor_list(RESISTOR_LIST)


value_combinations = itertools.product(values_r, values_r)
marking_combinations = itertools.product(markings, markings)

table = []

for markings, values in zip(marking_combinations, value_combinations):
    marking_high, marking_low = markings
    value_high, value_low = values


    table.append((fitness(value_high, value_low), 
                  fitness_string(value_high, value_low, marking_high, marking_low)))
    

sorted_table = sorted(table, key=lambda x:x[0], reverse=True)

for e in sorted_table:
    if(e[0][0] < PRINT_EFFICIENCY_THRES):
        continue
    print(e[1], e[0])
    
    

        
        
